# Web API and database with Spring

Java Assignment 3 at Noroff Accelerate.

## Table of contents
1. [Background](#background)
2. [Installation](#installation)
3. [Usage](#usage)
4. [Author](#author)

## Background
PostgreSQL database created with Hibernate and seeded with ApplicationRunner.
Exposed through Spring Web API.

The database contains characters, movies and franchises, and suitable relationships between them. 
Controllers contain generic CRUD and some additional endpoints for updating and reporting.

## Installation

* Install JDK 17
* Install IntelliJ IDEA
* PostgreSQL with PgAdmin
* Clone repository

## Usage

* Run the application through the main class and method
* See Swagger UI/Open API documentation for endpoints

## Author
Jo Endre Brauten Urdal