package no.noroff.javaassignment3.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Schema for movie: title, genres, year of release, director, picture URL, and trailer URL
 */
@Entity
public class Movie {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int id;
    private String title;
    private String genre;
    private int releaseYear;
    private String director;
    private String pictureURL;
    private String trailerURL;

    // One movie contains many characters, and a character can play in many movies -> ManyToMany
    // One movie belongs to one franchise, but a franchise can contain many movies -> ManyToOne (child side)
    // Relationships:
    @ManyToOne
    private Franchise franchises;
    @ManyToMany
    @JoinTable(
            name = "character_movie",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "character_id")}
    )
    private Set<Character> characters = new HashSet<>();

    public Movie() {
    }

    public Movie(String title, String genre, int releaseYear, String director, String pictureURL, String trailerURL, Franchise franchises, Set<Character> characters) {
        this.title = title;
        this.genre = genre;
        this.releaseYear = releaseYear;
        this.director = director;
        this.pictureURL = pictureURL;
        this.trailerURL = trailerURL;
        this.franchises = franchises;
        this.characters = characters;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(String pictureURL) {
        this.pictureURL = pictureURL;
    }

    public String getTrailerURL() {
        return trailerURL;
    }

    public void setTrailerURL(String trailerURL) {
        this.trailerURL = trailerURL;
    }

    @JsonGetter("franchises")
    public Franchise getFranchises() {
        return franchises;
    }

    public void setFranchise(Franchise franchises) {
        this.franchises = franchises;
    }

    @JsonGetter("characters")
    public List<Integer> getCharacters() {
        return characters.stream().map(Character::getId).collect(Collectors.toList());
    }

    public void setCharacters(Set<Character> characters) {
        this.characters = characters;
    }
}
