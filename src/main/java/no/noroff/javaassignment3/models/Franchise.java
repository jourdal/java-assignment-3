package no.noroff.javaassignment3.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Schema for franchise: name and description
 */
@Entity
public class Franchise {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String description;

    // One movie belongs to one franchise, but a franchise can contain many movies -> OneToMany (parent side)
    // Relationships:
    @OneToMany(mappedBy = "franchises")
    private List<Movie> movies;

    public Franchise() {
    }

    public Franchise(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonGetter("movies")
    public List<Integer> getMovies() {
        if (movies != null) {
            return movies.stream().map(Movie::getId).collect(Collectors.toList());
        } else {
            return null;
        }
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }
}
