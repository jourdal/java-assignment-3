package no.noroff.javaassignment3.models;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Schema for character: fullname, alias, gender, and picture URL
 */
@Entity
public class Character {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int id;
    private String fullName;
    private String alias;
    private boolean isFemale;
    private String pictureURL;

    // One movie contains many characters, and a character can play in many movies -> ManyToMany
    // Relationships:
    @ManyToMany
    @JoinTable(
            name = "character_movie",
            joinColumns = {@JoinColumn(name = "character_id")},
            inverseJoinColumns = {@JoinColumn(name = "movie_id")}
    )
    private Set<Movie> movies = new HashSet<>();

    public Character() {
    }

    public Character(String fullName, String alias, boolean isFemale, String pictureURL) {
        this.fullName = fullName;
        this.alias = alias;
        this.isFemale = isFemale;
        this.pictureURL = pictureURL;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public boolean isFemale() {
        return isFemale;
    }

    public void setFemale(boolean female) {
        isFemale = female;
    }

    public String getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(String pictureURL) {
        this.pictureURL = pictureURL;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }
}
