package no.noroff.javaassignment3.controllers;

import no.noroff.javaassignment3.models.Character;
import no.noroff.javaassignment3.models.Movie;
import no.noroff.javaassignment3.repositories.CharacterRepository;
import no.noroff.javaassignment3.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * Controller for movies: generic CRUD + gets character ids in movie by its id
 * + updates characters in a movie by its id with a body of an integer array of character ids
 */
@RestController
@RequestMapping("api/v1/movies")
public class MovieController {
    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private CharacterRepository characterRepository;

    /**
     * Gets all movies
     * @return
     */
    @GetMapping
    public ResponseEntity<List<Movie>> getAllMovies() {
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(movieRepository.findAll(),status);
    }

    /**
     * Gets all character ids in a movie by its id
     * @param id
     * @return
     */
    @GetMapping("characters/{id}")
    public ResponseEntity<List<Integer>> getMovieCharacters(@PathVariable int id) {
        HttpStatus status = HttpStatus.OK;
        List<Integer> characters = movieRepository.getById(id).getCharacters();
        return new ResponseEntity<>(characters, status);
    }

    /**
     * Creates a new movie with a body of a new movie
     * @param movie
     * @return
     */
    @PostMapping("/add")
    public ResponseEntity<Movie> addMovie(@RequestBody Movie movie){
        Movie returnMovie = movieRepository.save(movie);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnMovie, status);
    }

    /**
     * Updates a movie by its id in path with a body of a new movie
     * @param id
     * @param movie
     * @return
     */
    @PutMapping("/{id}")
    public ResponseEntity<Movie> updateMovie(@PathVariable Integer id, @RequestBody Movie movie) {
        Movie returnMovie = new Movie();
        HttpStatus status;

        // Checks for existing id
        if (!id.equals(movie.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnMovie,status);
        }

        // Updates movie
        returnMovie = movieRepository.save(movie);
        status = HttpStatus.NO_CONTENT;

        return new ResponseEntity<>(returnMovie, status);
    }

    /**
     * Updates characters in a movie by its id with a body of an integer array of character ids
     * @param id
     * @param characterIds
     * @return
     */
    @PatchMapping("/{id}")
    public ResponseEntity<Movie> updateCharacters(@PathVariable int id, @RequestBody int[] characterIds) {
        Movie returnMovie = movieRepository.getById(id);
        HttpStatus status = HttpStatus.OK;

        // Adds the character ids from the body to the movie
        Set<Character> characters = new HashSet<>();
        for (int i = 0; i < characterIds.length; i++) {
            characters.add(characterRepository.getById(characterIds[i]));
        }
        returnMovie.setCharacters(characters);

        return new ResponseEntity<>(movieRepository.save(returnMovie), status);
    }

    /**
     * Deletes movie by its id
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public Map<String, Boolean> deleteMovie(@PathVariable Integer id) {
        Movie returnMovie = movieRepository.getById(id);
        HttpStatus status;

        // Deletes movie and confirms the deletion
        movieRepository.delete(returnMovie);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", true);

        return response;
    }
}
