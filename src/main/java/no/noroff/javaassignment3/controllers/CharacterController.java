package no.noroff.javaassignment3.controllers;

import no.noroff.javaassignment3.models.Character;
import no.noroff.javaassignment3.repositories.CharacterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Controller for characters: generic CRUD
 */
@RestController
@RequestMapping("api/v1/characters")
public class CharacterController {

    @Autowired
    private CharacterRepository characterRepository;

    /**
     * Gets all characters
     * @return
     */
    @GetMapping
    public ResponseEntity<List<Character>> getAllCharacters() {
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(characterRepository.findAll(),status);
    }

    /**
     * Creates a new character with a body of a new character
     * @param character
     * @return
     */
    @PostMapping("/add")
    public ResponseEntity<Character> addCharacter(@RequestBody Character character){
        Character returnCharacter = characterRepository.save(character);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnCharacter, status);
    }

    /**
     * Updates a character by its id in path with a body of a new character
     * @param id
     * @param character
     * @return
     */
    @PutMapping("/{id}")
    public ResponseEntity<Character> updateCharacter(@PathVariable Integer id, @RequestBody Character character) {
        Character returnCharacter = new Character();
        HttpStatus status;

        // Checks for existing id
        if (!id.equals(character.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnCharacter,status);
        }

        // Updates character
        returnCharacter = characterRepository.save(character);
        status = HttpStatus.NO_CONTENT;

        return new ResponseEntity<>(returnCharacter, status);
    }

    /**
     * Deletes character by its id
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public Map<String, Boolean> deleteCharacter(@PathVariable Integer id) {
        Character returnCharacter = characterRepository.getById(id);
        HttpStatus status;

        // Deletes character and confirms the deletion
        characterRepository.delete(returnCharacter);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", true);

        return response;
    }
}
