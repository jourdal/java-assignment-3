package no.noroff.javaassignment3.controllers;

import no.noroff.javaassignment3.models.Franchise;
import no.noroff.javaassignment3.models.Movie;
import no.noroff.javaassignment3.repositories.FranchiseRepository;
import no.noroff.javaassignment3.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * Controller for franchises: generic CRUD + gets movie ids in franchise by its id
 * + gets character ids in franchise by its id
 * + updates movies in a franchise by its id with a body of an integer array of movie ids
 */
@RestController
@RequestMapping("api/v1/franchises")
public class FranchiseController {
    @Autowired
    private FranchiseRepository franchiseRepository;
    @Autowired
    private MovieRepository movieRepository;

    /**
     * Gets all franchises
     * @return
     */
    @GetMapping
    public ResponseEntity<List<Franchise>> getAllFranchises() {
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(franchiseRepository.findAll(),status);
    }

    /**
     * Gets all movie ids in a franchise by its id
     * @param id
     * @return
     */
    @GetMapping("movies/{id}")
    public ResponseEntity<List<Integer>> getFranchiseMovies(@PathVariable int id) {
        HttpStatus status = HttpStatus.OK;
        List<Integer> movies = franchiseRepository.getById(id).getMovies();
        return new ResponseEntity<>(movies, status);
    }

    /**
     * Gets all character ids in a franchise by its id
     * @param id
     * @return
     */
    @GetMapping("characters/{id}")
    public ResponseEntity<List<Integer>> getFranchiseCharacter(@PathVariable int id) {
        HttpStatus status = HttpStatus.OK;

        Franchise franchise = franchiseRepository.getById(id);
        List<Integer> characters = new ArrayList<>();

        // Nested for-loop that gets a list of all characters in all movies of the franchise, one at a time
        for (int i = 0; i < franchise.getMovies().size(); i++) {
            List<Integer> movieCharacters = movieRepository.getById(franchise.getMovies().get(i)).getCharacters();
            for (int j = 0; j < movieCharacters.size(); j++) {
                if (!characters.contains(movieCharacters.get(j))) {
                    characters.add(movieCharacters.get(j));
                }
            }
        }

        return new ResponseEntity<>(characters, status);
    }

    /**
     * Creates a new franchise with a body of a new franchise
     * @param franchise
     * @return
     */
    @PostMapping("/add")
    public ResponseEntity<Franchise> addFranchise(@RequestBody Franchise franchise){
        Franchise returnFranchise = franchiseRepository.save(franchise);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnFranchise, status);
    }

    /**
     * Updates a franchise by its id in path with a body of a new franchise
     * @param id
     * @param franchise
     * @return
     */
    @PutMapping("/{id}")
    public ResponseEntity<Franchise> updateFranchise(@PathVariable Integer id, @RequestBody Franchise franchise) {
        Franchise returnFranchise = new Franchise();
        HttpStatus status;

        // Checks for existing id
        if(!id.equals(franchise.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnFranchise,status);
        }

        // Updates franchise
        returnFranchise = franchiseRepository.save(franchise);
        status = HttpStatus.NO_CONTENT;

        return new ResponseEntity<>(returnFranchise, status);
    }

    /**
     * Updates movies in a franchise by its id with a body of an integer array of movie ids
     * @param id
     * @param movieIds
     * @return
     */
    @PatchMapping("/{id}")
    public ResponseEntity<Franchise> updateMovies(@PathVariable int id, @RequestBody int[] movieIds) {
        Franchise returnFranchise = franchiseRepository.getById(id);
        HttpStatus status = HttpStatus.OK;

        // Adds the movie ids from the body to the franchise and also updates the respective movie franchises
        List<Movie> movies = new ArrayList<>();
        for (int i = 0; i < movieIds.length; i++) {
            Movie movie = movieRepository.getById(movieIds[i]);
            movie.setFranchise(returnFranchise);
            movies.add(movie);
        }
        returnFranchise.setMovies(movies);

        return new ResponseEntity<>(franchiseRepository.save(returnFranchise), status);
    }

    /**
     * Sets franchise to null in all movies of that franchise and deletes franchise by its id
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public Map<String, Boolean> deleteFranchise(@PathVariable Integer id) {
        Franchise returnFranchise = franchiseRepository.getById(id);

        // Gets all movies in the franchise and sets franchise to null for those movies
        List<Integer> franchiseMovies = returnFranchise.getMovies();
        for (int i = 0; i < franchiseMovies.size(); i++) {
            Movie movie = movieRepository.getById(franchiseMovies.get(i));
            movie.setFranchise(null);
        }

        // Deletes franchise and confirms the deletion
        franchiseRepository.delete(returnFranchise);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", true);

        return response;
    }
}
