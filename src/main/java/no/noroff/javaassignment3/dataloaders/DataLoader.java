package no.noroff.javaassignment3.dataloaders;

import no.noroff.javaassignment3.models.Character;
import no.noroff.javaassignment3.models.Franchise;
import no.noroff.javaassignment3.models.Movie;
import no.noroff.javaassignment3.repositories.CharacterRepository;
import no.noroff.javaassignment3.repositories.FranchiseRepository;
import no.noroff.javaassignment3.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * Seeds dummy data to the PostgreSQL database using ApplicationRunner
 */
@Component
public class DataLoader implements ApplicationRunner {

    @Autowired
    CharacterRepository characterRepository;
    @Autowired
    MovieRepository movieRepository;
    @Autowired
    FranchiseRepository franchiseRepository;


    @Override
    public void run(ApplicationArguments args) throws Exception {
        loadData();
    }

    private void loadData() {
        if (movieRepository.count() == 0 && characterRepository.count() == 0 && franchiseRepository.count() == 0) {
            final Franchise franchise1 = franchiseRepository.save(new Franchise("Lord of the Rings", "Lord of the Rings"));
            final Franchise franchise2 = franchiseRepository.save(new Franchise("MCU", "Marvel Cinematic Universe"));
            final Character character1 = characterRepository.save(new Character("Elijah Wood", "Bilbo Baggins", false,
                    "https://m.media-amazon.com/images/M/MV5BMTM0NDIxMzQ5OF5BMl5BanBnXkFtZTcwNzAyNTA4Nw@@._V1_UX214_CR0,0,214,317_AL_.jpg"));
            final Character character2 = characterRepository.save(new Character("Ian McKellen", "Gandalf", false,
                    "https://www.imdb.com/name/nm0005212/mediaviewer/rm230854144/"));
            final Character character3 = characterRepository.save(new Character("Scarlet Johansen", "Natasha Romanoff", true,
                    "https://www.imdb.com/name/nm0424060/mediaviewer/rm1916122112?ref_=nm_ov_ph"));
            final Character character4 = characterRepository.save(new Character("Chris Evans", "Captain America", false,
                    "https://www.imdb.com/name/nm0262635/mediaviewer/rm1966443008?ref_=nm_ov_ph"));
            final Movie movie1 = movieRepository.save(new Movie("The Lord of the Rings: The Fellowship of the Ring", "action, genre, drama", 2001,
                    "Peter Jackson", "https://www.imdb.com/title/tt0120737/mediaviewer/rm3592958976/",
                    "https://www.imdb.com/video/vi684573465?playlistId=tt0120737&ref_=tt_ov_vi", franchiseRepository.getById(1),
                    Set.of(character1, character2)));
            final Movie movie2 = movieRepository.save(new Movie("The Hobbit: An Unexpected Journey", "adventure, fantasy", 2012, "Peter Jackson",
                    "https://www.imdb.com/title/tt0903624/mediaviewer/rm3577719808/",
                    "https://www.imdb.com/video/vi650683417?playlistId=tt0903624&ref_=tt_ov_vi", franchiseRepository.getById(1),
                    Set.of(character1, character2)));
            final Movie movie3 = movieRepository.save(new Movie("The Avengers", "action, adventure, sci-fi", 2012, "Joss Whedon",
                    "https://www.imdb.com/title/tt0848228/mediaviewer/rm3955117056/",
                    "https://www.imdb.com/video/vi1891149081?playlistId=tt0848228&ref_=tt_ov_vi", franchiseRepository.getById(2),
                    Set.of(character3, character4)));
        }
    }
}
